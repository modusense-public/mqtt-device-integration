# ModuSense MQTT Device Integration Examples

This repository provides example golang code and how a device might would
integrate with the ModuSense MQTT Broker.

Each example script includes inline comments to further document each case.

# Prerequisites

### Device Certificate

The example scripts require that a device certificate and private key are
accessible in the root directory.

e.g.

```
.
├── **certificate.pem.crt**
├── **private.pem.key**
├── configuration_retrieve_example.go
├── go.mod
├── go.sum
├── publish_measurement_example.go
└── README.md
```

# Examples

### Publishing Measurements

```
go run ./publish_measurement_example.go
```

### Retrieving/Subscribing to Device Configuration

```
go run ./configuration_retrieve_example.go
```

