/*

This example supercedes example_subscribe.go since we want to move from the
retained message model due to scaling issues.

**THIS IS NOT CURRENTLY ACTIVE.**


This example demonstrates the following:

 - Creating a secure TLS connection to ModuSense's (staging) IoT Core instance
 - Subscription to the ModuSense configuration topic
 - Reception and handling of the configuration payload

See example_publish.go for documentation on environment variables and setting
up certificates to ensure correct access.

# Examples

All the following examples use the certificate for TAD001.

## Simple (Success)

```
$ go run example_subscribe_revised.go
2022/04/08 10:42:59 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 10:42:59 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/08 10:43:01 connected
2022/04/08 10:43:01 subscribed to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/accepted
2022/04/08 10:43:01 subscribed to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/rejected
2022/04/08 10:43:01 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/request (qos = 1)
2022/04/08 10:43:03 received message (OK)
topic    device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/accepted
qos      0
retained false
payload
==
{
  "example-key1": "example-value1",
  "example-key2": "example-value2"
}
==
```

## Simple (Failure)

```
$ go run example_subscribe_revised.go
2022/04/08 10:42:16 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 10:42:16 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/08 10:42:16 connected
2022/04/08 10:42:17 subscribed to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/accepted
2022/04/08 10:42:17 subscribed to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/rejected
2022/04/08 10:42:17 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/request (qos = 1)
2022/04/08 10:42:19 received message (ERROR)
topic    device/392473b4-f875-430b-b6e2-f4aee7afcc7a/sb/configuration/rejected
qos      0
retained false
payload
==
{
  "error": "no configuration for device"
}
==
```

*/

package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"os"

	"github.com/cenkalti/backoff"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/kelseyhightower/envconfig"
)

var (
	config Config
	client mqtt.Client
)

type Config struct {
	DeviceId       string `default:"392473b4-f875-430b-b6e2-f4aee7afcc7a" split_words:"true"`                        // TAD001
	SubscribeQos   byte   `default:"1" split_words:"true"`                                                           // 0 or 1 supported
	PublishQos     byte   `default:"1" split_words:"true"`                                                           // 0 or 1 supported
	MqttConn       string `default:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883" split_words:"true"` // Staging ModuSense Endpoint
	MqttClientId   string `default:"modusense-test-client1" split_words:"true"`
	MqttUsername   string `default:"" split_words:"true"`
	MqttPassword   string `default:"" split_words:"true"`
	MqttDebug      bool   `default:"false" split_words:"true"`
	MqttNoTls      bool   `default:"false" split_words:"true"`
	MqttSkipVerify bool   `default:"false" split_words:"true"` // Server Certificate Verification
	MqttClientCert string `default:"certificate.pem.crt" split_words:"true"`
	MqttPrivateKey string `default:"private.pem.key" split_words:"true"`
}

func newMqttTlsConfig(config *Config) (*tls.Config, error) {
	var err error
	var certpool *x509.CertPool

	// use system CA
	certpool, err = x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	clientKeyPair, err := tls.LoadX509KeyPair(config.MqttClientCert, config.MqttPrivateKey)
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: config.MqttSkipVerify,
		Certificates:       []tls.Certificate{clientKeyPair},
		MinVersion:         tls.VersionTLS12, // Required for AWS
	}, nil
}

func NewMqttClient(config *Config) mqtt.Client {
	if config.MqttDebug {
		mqtt.ERROR = log.New(os.Stdout, "[mqtt/error] ", 0)
		mqtt.CRITICAL = log.New(os.Stdout, "[mqtt/critical] ", 0)
		mqtt.WARN = log.New(os.Stdout, "[mqtt/warn] ", 0)
		mqtt.DEBUG = log.New(os.Stdout, "[mqtt/debug] ", 0)
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(config.MqttConn)
	opts.SetClientID(config.MqttClientId)
	opts.SetUsername(config.MqttUsername)
	opts.SetPassword(config.MqttPassword)
	opts.SetOrderMatters(false)
	opts.SetAutoReconnect(true)

	if !config.MqttNoTls {
		tlsConfig, err := newMqttTlsConfig(config)
		if err != nil {
			log.Fatal(err.Error())
		}
		opts.SetTLSConfig(tlsConfig)
	}

	client := mqtt.NewClient(opts)

	backoff.Retry(func() error {
		log.Printf("connect attempt to %s", opts.Servers[0])
		token := client.Connect()
		token.Wait()
		return token.Error()
	}, backoff.NewExponentialBackOff())

	log.Printf("connected")

	return client
}

// This handler is called on each successful configuration response.
func configurationSuccessHandler(c mqtt.Client, m mqtt.Message) {
	log.Printf(`received message (OK)
topic    %s
qos      %d
retained %t
payload
==
%s
==`, m.Topic(), m.Qos(), m.Retained(), m.Payload())

	//Process configuration and update local config

	//Send acknowldgement so the server knows your processed
	configurationAckowledge()
}

// This handler is called when there was an error from the configuration endpoint.
func configurationFailureHandler(c mqtt.Client, m mqtt.Message) {
	log.Printf(`received message (ERROR)
topic    %s
qos      %d
retained %t
payload
==
%s
==`, m.Topic(), m.Qos(), m.Retained(), m.Payload())

	//no good configuration received, continue using old

	//Send acknowldgement so the server knows your processed
	configurationAckowledge()
}

func configurationAckowledge() {
	// mqtt publishing parameters
	publishTopic := fmt.Sprintf("device/%s/sb/configuration/ack", config.DeviceId)
	isRetained := false
	payload := "{}" // payload should be valid JSON, content isn't processed currently

	token := client.Publish(publishTopic, config.PublishQos, isRetained, []byte(payload))
	token.Wait()

	if err := token.Error(); err != nil {
		log.Fatalf("failed to publish to %s: %s", publishTopic, err)
	}

	log.Printf("published message to %s (qos = %d)", publishTopic, config.PublishQos)
}

func main() {
	err := envconfig.Process("ENV", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Printf("%+#v", config)

	client = NewMqttClient(&config)

	// subscribe to topic listening for success (valid configuration)
	{
		subscribeTopic := fmt.Sprintf("device/%s/sb/configuration/accepted", config.DeviceId)

		token := client.Subscribe(subscribeTopic, config.SubscribeQos, configurationSuccessHandler)
		token.Wait()
		if err := token.Error(); err != nil {
			log.Fatalf("failed to subscribe to %s: %s", subscribeTopic, err)
		}

		log.Printf("subscribed to %s", subscribeTopic)
	}

	// subscribe to topic listening for failure (no configuration/error)
	{
		subscribeTopic := fmt.Sprintf("device/%s/sb/configuration/rejected", config.DeviceId)

		token := client.Subscribe(subscribeTopic, config.SubscribeQos, configurationFailureHandler)
		token.Wait()
		if err := token.Error(); err != nil {
			log.Fatalf("failed to subscribe to %s: %s", subscribeTopic, err)
		}

		log.Printf("subscribed to %s", subscribeTopic)
	}

	// client drives the configuration process by writing to request topic
	{
		// mqtt publishing parameters
		publishTopic := fmt.Sprintf("device/%s/sb/configuration/request", config.DeviceId)
		isRetained := false
		payload := "{}" // payload should be valid JSON, content isn't processed currently

		token := client.Publish(publishTopic, config.PublishQos, isRetained, []byte(payload))
		token.Wait()

		if err := token.Error(); err != nil {
			log.Fatalf("failed to publish to %s: %s", publishTopic, err)
		}

		log.Printf("published message to %s (qos = %d)", publishTopic, config.PublishQos)
	}

	// wait indefinitely for messages until process is terminated. In practice
	// a timeout can be used for ephemeral clients.
	select {}
}
