/*

This example demonstrates retrieving southbound messages via the generic southbound
topics `device/+/sb2/*`.

This example demonstrates the following:

 - Creating a secure TLS connection to ModuSense's (staging) IoT Core instance
 - Subscription to the ModuSense southbound topic
 - Reception and handling of the southbound payload

See publish_measurement_example.go for documentation on environment variables and setting
up certificates to ensure correct access.

# Example

# Request a single queued message

```
$ ENV_DEVICE_ID=00000000-0000-0000-0000-000000000001 go run southbound_example.go
2022/04/28 10:42:19 main.Config{DeviceId:"00000000-0000-0000-0000-000000000001", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/28 10:42:19 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/28 10:42:20 connected
2022/04/28 10:42:20 subscribed to device/00000000-0000-0000-0000-000000000001/sb2
request
2022/04/28 10:42:24 ACTION: send request to send queued message
2022/04/28 10:42:24 publish to device/00000000-0000-0000-0000-000000000001/sb2/request
2022/04/28 10:42:24 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==
$ref:A461F
test command
==
2022/04/28 10:42:24 USER: processing data (length = 12)
2022/04/28 10:42:24 publish to device/00000000-0000-0000-0000-000000000001/sb2/ack
2022/04/28 10:42:25 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==

==
2022/04/28 10:42:25 DONE: no messages remaining
```

# Request multiple queued messages

```
$ ENV_DEVICE_ID=00000000-0000-0000-0000-000000000001 go run southbound_example.go
2022/04/28 10:43:20 main.Config{DeviceId:"00000000-0000-0000-0000-000000000001", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/28 10:43:20 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/28 10:43:21 connected
2022/04/28 10:43:21 subscribed to device/00000000-0000-0000-0000-000000000001/sb2
request
2022/04/28 10:43:24 ACTION: send request to send queued message
2022/04/28 10:43:24 publish to device/00000000-0000-0000-0000-000000000001/sb2/request
2022/04/28 10:43:24 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==
$ref:13C37
test command
==
2022/04/28 10:43:24 USER: processing data (length = 12)
2022/04/28 10:43:24 publish to device/00000000-0000-0000-0000-000000000001/sb2/ack
2022/04/28 10:43:25 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==
$ref:4E5BC
test command
==
2022/04/28 10:43:25 USER: processing data (length = 12)
2022/04/28 10:43:25 publish to device/00000000-0000-0000-0000-000000000001/sb2/ack
2022/04/28 10:43:25 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==
$ref:7DAF0
test command
==
2022/04/28 10:43:25 USER: processing data (length = 12)
2022/04/28 10:43:25 publish to device/00000000-0000-0000-0000-000000000001/sb2/ack
2022/04/28 10:43:25 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==

==
2022/04/28 10:43:25 DONE: no messages remaining
```

# Receive new message when published immediately

```
$ ENV_DEVICE_ID=00000000-0000-0000-0000-000000000001 go run southbound_example.go
2022/04/28 10:43:55 main.Config{DeviceId:"00000000-0000-0000-0000-000000000001", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/28 10:43:55 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/28 10:43:56 connected
2022/04/28 10:43:56 subscribed to device/00000000-0000-0000-0000-000000000001/sb2
request
2022/04/28 10:43:58 ACTION: send request to send queued message
2022/04/28 10:43:58 publish to device/00000000-0000-0000-0000-000000000001/sb2/request
2022/04/28 10:43:59 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==

==
2022/04/28 10:43:59 DONE: no messages remaining
2022/04/28 10:44:10 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==
$ref:D657D
test command
==
2022/04/28 10:44:10 USER: processing data (length = 12)
2022/04/28 10:44:10 publish to device/00000000-0000-0000-0000-000000000001/sb2/ack
2022/04/28 10:44:11 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==

==
2022/04/28 10:44:11 DONE: no messages remaining
```

# Request with no messages available

```
$ ENV_DEVICE_ID=00000000-0000-0000-0000-000000000001 go run southbound_example.go
2022/04/28 10:44:38 main.Config{DeviceId:"00000000-0000-0000-0000-000000000001", SubscribeQos:0x1, PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/28 10:44:38 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/28 10:44:39 connected
2022/04/28 10:44:39 subscribed to device/00000000-0000-0000-0000-000000000001/sb2
request
2022/04/28 10:44:42 ACTION: send request to send queued message
2022/04/28 10:44:42 publish to device/00000000-0000-0000-0000-000000000001/sb2/request
2022/04/28 10:44:42 received message
topic    device/00000000-0000-0000-0000-000000000001/sb2
qos      1
retained false
payload
==

==
2022/04/28 10:44:42 DONE: no messages remaining
```

*/

package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
	"unicode/utf8"

	"github.com/cenkalti/backoff"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/kelseyhightower/envconfig"
)

var (
	config Config
	client mqtt.Client
)

type Config struct {
	DeviceId       string `default:"392473b4-f875-430b-b6e2-f4aee7afcc7a" split_words:"true"`                        // TAD001
	SubscribeQos   byte   `default:"1" split_words:"true"`                                                           // 0 or 1 supported
	PublishQos     byte   `default:"1" split_words:"true"`                                                           // 0 or 1 supported
	MqttConn       string `default:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883" split_words:"true"` // Staging ModuSense Endpoint
	MqttClientId   string `default:"modusense-test-client1" split_words:"true"`
	MqttUsername   string `default:"" split_words:"true"`
	MqttPassword   string `default:"" split_words:"true"`
	MqttDebug      bool   `default:"false" split_words:"true"`
	MqttNoTls      bool   `default:"false" split_words:"true"`
	MqttSkipVerify bool   `default:"false" split_words:"true"` // Server Certificate Verification
	MqttClientCert string `default:"certificate.pem.crt" split_words:"true"`
	MqttPrivateKey string `default:"private.pem.key" split_words:"true"`
}

func newMqttTlsConfig(config *Config) (*tls.Config, error) {
	var err error
	var certpool *x509.CertPool

	// use system CA
	certpool, err = x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	clientKeyPair, err := tls.LoadX509KeyPair(config.MqttClientCert, config.MqttPrivateKey)
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: config.MqttSkipVerify,
		Certificates:       []tls.Certificate{clientKeyPair},
		MinVersion:         tls.VersionTLS12, // Required for AWS
	}, nil
}

func NewMqttClient(config *Config) mqtt.Client {
	if config.MqttDebug {
		mqtt.ERROR = log.New(os.Stdout, "[mqtt/error] ", 0)
		mqtt.CRITICAL = log.New(os.Stdout, "[mqtt/critical] ", 0)
		mqtt.WARN = log.New(os.Stdout, "[mqtt/warn] ", 0)
		mqtt.DEBUG = log.New(os.Stdout, "[mqtt/debug] ", 0)
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(config.MqttConn)
	opts.SetClientID(config.MqttClientId)
	opts.SetUsername(config.MqttUsername)
	opts.SetPassword(config.MqttPassword)
	opts.SetOrderMatters(false)
	opts.SetAutoReconnect(true)

	if !config.MqttNoTls {
		tlsConfig, err := newMqttTlsConfig(config)
		if err != nil {
			log.Fatal(err.Error())
		}
		opts.SetTLSConfig(tlsConfig)
	}

	client := mqtt.NewClient(opts)

	backoff.Retry(func() error {
		log.Printf("connect attempt to %s", opts.Servers[0])
		token := client.Connect()
		token.Wait()
		return token.Error()
	}, backoff.NewExponentialBackOff())

	log.Printf("connected")

	return client
}

// First header-line is guaranteed to be UTF-8, terminated by a newline.
// Subsequent data is free-form bytes and is used-specified.
//
//	```
//	$ref:1234,header2:1234
//	DATA
//	```
type SouthboundPayload struct {
	Headers map[string]string
	Data    []byte
}

func (s *SouthboundPayload) Get(key string) (val string, ok bool) {
	val, ok = s.Headers[key]
	return
}

func (s *SouthboundPayload) Dump() {
	fmt.Printf(`H> %+v
B> %s
`, s.Headers, s.Data)
}

func ParseSouthboundPayload(m []byte) (*SouthboundPayload, error) {
	parts := bytes.SplitN(m, []byte{'\n'}, 2)
	if len(parts) != 2 {
		return nil, errors.New("header is not terminated by newline")
	}

	rawHeader := bytes.TrimSuffix(parts[0], []byte{'\n'})
	if !utf8.Valid(rawHeader) {
		return nil, errors.New("header is not valid utf8")
	}

	records := []string{}
	rh := string(rawHeader)
	sb := strings.Builder{}
	inEscape := false
	for _, c := range rh {
		if inEscape {
			inEscape = false
			sb.WriteRune(c)
		} else if c == '\\' {
			inEscape = true
		} else if c == ',' {
			records = append(records, sb.String())
			sb = strings.Builder{} // preserve existing memory above
		} else {
			sb.WriteRune(c)
		}
	}
	records = append(records, sb.String())

	payload := &SouthboundPayload{
		Headers: make(map[string]string),
		Data:    parts[1],
	}

	// e.g. `$ref:1234`
	for _, header := range records {
		parts := strings.SplitN(header, ":", 2)
		if len(parts) != 2 {
			return nil, errors.New("key-value pair is not separated by ':'")
		}
		if len(parts[0]) == 0 || parts[0][0] != '$' {
			return nil, errors.New("key is not prefixed with '$'")
		}

		key := parts[0][1:]
		val := parts[1]
		payload.Headers[key] = val
	}

	return payload, nil
}

const (
	NOT_RETAINED = false
)

// Called when a new message is sent
func subscriptionHandler(c mqtt.Client, m mqtt.Message) {
	log.Printf(`received message
topic    %s
qos      %d
retained %t
payload
==
%s
==`, m.Topic(), m.Qos(), m.Retained(), m.Payload())

	if len(m.Payload()) == 0 {
		log.Printf("DONE: no messages remaining")
		m.Ack()
		return
	}

	payload, err := ParseSouthboundPayload(m.Payload())
	if err != nil {
		log.Printf("failed to process southbound message: %s", err.Error()) // contact support
		return
	}

	ref, found := payload.Get("ref")
	if !found {
		log.Printf("no reference key found") // contact support
		return
	}

	// User performs any message processing here on the body
	// NOTE: Body is not necessarily UTF-8 encoded. We assume here for simplicity
	// but the encoded is user-specified (beyond the header).
	log.Printf("USER: processing data (length = %d)", len(payload.Data))

	// Acknowledge message to clear from queue. Can acknowledge if an error occurs.
	// NOTE: This acknowledgement message queues the next downstream message.
	// Another explicit request is not required.
	ackBody := fmt.Sprintf("$ref:%s,$status:%s\n", ref, "success")

	publishTopic := fmt.Sprintf("device/%s/sb2/ack", config.DeviceId)
	log.Printf("publish to %s", publishTopic)

	token := c.Publish(publishTopic, config.PublishQos, NOT_RETAINED, []byte(ackBody))
	token.Wait()
	if err := token.Error(); err != nil {
		log.Printf("failed to publish acknowledgement: %s", err.Error())
		return
	}

	m.Ack()
}

func main() {
	err := envconfig.Process("ENV", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Printf("%+#v", config)

	client = NewMqttClient(&config)

	// subscribe to southbound topic reception
	{
		subscribeTopic := fmt.Sprintf("device/%s/sb2", config.DeviceId)

		token := client.Subscribe(subscribeTopic, config.SubscribeQos, subscriptionHandler)
		token.Wait()
		if err := token.Error(); err != nil {
			log.Fatalf("failed to subscribe to %s: %s", subscribeTopic, err)
		}

		log.Printf("subscribed to %s", subscribeTopic)
	}

	// Each time user enters line, all existing message content will be read
	reader := bufio.NewReader(os.Stdin)
	for {
		_, _ = reader.ReadString('\n')
		log.Printf("ACTION: send request to send queued message")

		requestTopic := fmt.Sprintf("device/%s/sb2/request", config.DeviceId)
		log.Printf("publish to %s", requestTopic)

		token := client.Publish(requestTopic, config.PublishQos, NOT_RETAINED, []byte{})
		token.Wait()
		if err := token.Error(); err != nil {
			log.Fatalf("failed to publish to %s: %s", requestTopic, err)
		}
	}
}
