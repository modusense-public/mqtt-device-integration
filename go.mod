module modusense/mqtt/examples

go 1.17

require (
	github.com/cenkalti/backoff v2.2.1+incompatible
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/kelseyhightower/envconfig v1.4.0
)

require (
	github.com/gorilla/websocket v1.4.2 // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
)
