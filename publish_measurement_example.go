/*

This example demonstrates the following:

 - Creating a secure TLS connection to ModuSense's (staging) IoT Core instance
 - Construction of a JSON message compatible with ModuSense's format
 - Publishing of a message to the ModuSense endpoint

The core logic is in the main function. NewMqttClient exemplifies how to set up
an MQTT connection over TLS.

# Environment Variables

All parameters can be changed with environment variables. These are documented
in the Config struct below, along with their defaults. These must be prefixed
by `ENV_` and each word is split by a `_`. For example to change the DeviceId
set the ENV_DEVICE_ID environment variable.

The configuration parameters are printed before each invocation for clarity.

# Running

This program requires device certificates to be present in the current working
directory in order to successfully communicate with ModuSense.

If you are using the certificates folder previously generated one way to set
this up is as follows:

```
# in this project directory, first ensure the certificates zip is unzipped and present
cd certificates/\(TAD001\)\ 392473b4-f875-430b-b6e2-f4aee7afcc7a

# run the publisher, setting the device id to the one we are transmitting as
ENV_DEVICE_ID=392473b4-f875-430b-b6e2-f4aee7afcc7a go run ../../example_publish.go
```

# Examples

All the following examples use the certificate for TAD001.

## Simple

```
$ go run example_publish.go
2022/04/08 09:55:24 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 09:55:24 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/08 09:55:25 connected
2022/04/08 09:55:25 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/nb/json (qos = 1)
```

## More MQTT debugging

```
$ ENV_MQTT_DEBUG=true go run example_publish.go
2022/04/08 09:56:34 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", PublishQos:0x1, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:true, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 09:56:34 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
[mqtt/debug] [client]   Connect()
[mqtt/debug] [store]    memorystore initialized
[mqtt/debug] [client]   about to write new connect msg
[mqtt/debug] [client]   socket connected to broker
[mqtt/debug] [client]   Using MQTT 3.1.1 protocol
[mqtt/debug] [net]      connect started
[mqtt/debug] [net]      received connack
[mqtt/debug] [client]   startCommsWorkers called
[mqtt/debug] [client]   client is connected/reconnected
[mqtt/debug] [net]      incoming started
[mqtt/debug] [net]      startIncomingComms started
[mqtt/debug] [net]      outgoing started
[mqtt/debug] [net]      startComms started
[mqtt/debug] [client]   startCommsWorkers done
[mqtt/warn] [store]    memorystore wiped
[mqtt/debug] [client]   exit startClient
2022/04/08 09:56:35 connected
[mqtt/debug] [client]   enter Publish
[mqtt/debug] [client]   sending publish message, topic: device/392473b4-f875-430b-b6e2-f4aee7afcc7a/nb/json
[mqtt/debug] [pinger]   keepalive starting
[mqtt/debug] [net]      logic waiting for msg on ibound
[mqtt/debug] [net]      startIncomingComms: inboundFromStore complete
[mqtt/debug] [net]      logic waiting for msg on ibound
[mqtt/debug] [net]      outgoing waiting for an outbound message
[mqtt/debug] [net]      obound msg to write 1
[mqtt/debug] [net]      obound wrote msg, id: 1
[mqtt/debug] [net]      outgoing waiting for an outbound message
[mqtt/debug] [net]      startIncoming Received Message
[mqtt/debug] [net]      startIncomingComms: got msg on ibound
[mqtt/debug] [store]    memorystore del: message 1 was deleted
[mqtt/debug] [net]      startIncomingComms: received puback, id: 1
[mqtt/debug] [net]      logic waiting for msg on ibound
2022/04/08 09:56:35 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/nb/json (qos = 1)
```

## Use QOS 0 during publish

```
$ ENV_PUBLISH_QOS=0 go run example_publish.go
2022/04/08 09:57:50 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", PublishQos:0x0, MqttConn:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883", MqttClientId:"modusense-test-client1", MqttUsername:"", MqttPassword:"", MqttDebug:false, MqttNoTls:false, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 09:57:50 connect attempt to mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883
2022/04/08 09:57:51 connected
2022/04/08 09:57:51 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/nb/json (qos = 0)
```


## Communicate with a different, public broker with credentials

```
$ ENV_MQTT_CONN=mqtt://test.mosquitto.org:1884 \
ENV_MQTT_USERNAME=rw \
ENV_MQTT_PASSWORD=readwrite \
ENV_MQTT_NO_TLS=true \
go run example_publish.go
2022/04/08 10:02:01 main.Config{DeviceId:"392473b4-f875-430b-b6e2-f4aee7afcc7a", PublishQos:0x1, MqttConn:"mqtt://test.mosquitto.org:1884", MqttClientId:"modusense-test-client1", MqttUsername:"rw", MqttPassword:"readwrite", MqttDebug:false, MqttNoTls:true, MqttSkipVerify:false, MqttClientCert:"certificate.pem.crt", MqttPrivateKey:"private.pem.key"}
2022/04/08 10:02:01 connect attempt to mqtt://test.mosquitto.org:1884
2022/04/08 10:02:02 connected
2022/04/08 10:02:02 published message to device/392473b4-f875-430b-b6e2-f4aee7afcc7a/nb/json (qos = 1)
```

*/

package main

import (
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"log"
	"math"
	"os"
	"time"

	"github.com/cenkalti/backoff"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	DeviceId       string `default:"cfbe34e5-ff54-4fae-8150-9c2dfec964a4" split_words:"true"`                        // TAD001
	PublishQos     byte   `default:"1" split_words:"true"`                                                           // 0 or 1 supported
	MqttConn       string `default:"mqtts://a3t99ov1reqc54-ats.iot.us-west-1.amazonaws.com:8883" split_words:"true"` // Staging ModuSense Endpoint
	MqttClientId   string `default:"modusense-test-client1" split_words:"true"`
	MqttUsername   string `default:"" split_words:"true"`
	MqttPassword   string `default:"" split_words:"true"`
	MqttDebug      bool   `default:"false" split_words:"true"`
	MqttNoTls      bool   `default:"false" split_words:"true"`
	MqttSkipVerify bool   `default:"false" split_words:"true"` // Server Certificate Verification
	MqttClientCert string `default:"certificate.pem.crt" split_words:"true"`
	MqttPrivateKey string `default:"private.pem.key" split_words:"true"`
}

func newMqttTlsConfig(config *Config) (*tls.Config, error) {
	var err error
	var certpool *x509.CertPool

	// use system CA
	certpool, err = x509.SystemCertPool()
	if err != nil {
		return nil, err
	}

	clientKeyPair, err := tls.LoadX509KeyPair(config.MqttClientCert, config.MqttPrivateKey)
	if err != nil {
		return nil, err
	}

	return &tls.Config{
		RootCAs:            certpool,
		ClientAuth:         tls.NoClientCert,
		ClientCAs:          nil,
		InsecureSkipVerify: config.MqttSkipVerify,
		Certificates:       []tls.Certificate{clientKeyPair},
		MinVersion:         tls.VersionTLS12, // Required for AWS
	}, nil
}

func NewMqttClient(config *Config) mqtt.Client {
	if config.MqttDebug {
		mqtt.ERROR = log.New(os.Stdout, "[mqtt/error] ", 0)
		mqtt.CRITICAL = log.New(os.Stdout, "[mqtt/critical] ", 0)
		mqtt.WARN = log.New(os.Stdout, "[mqtt/warn] ", 0)
		mqtt.DEBUG = log.New(os.Stdout, "[mqtt/debug] ", 0)
	}

	opts := mqtt.NewClientOptions()
	opts.AddBroker(config.MqttConn)
	opts.SetClientID(config.MqttClientId)
	opts.SetUsername(config.MqttUsername)
	opts.SetPassword(config.MqttPassword)
	opts.SetOrderMatters(false)
	opts.SetAutoReconnect(true)

	if !config.MqttNoTls {
		tlsConfig, err := newMqttTlsConfig(config)
		if err != nil {
			log.Fatal(err.Error())
		}
		opts.SetTLSConfig(tlsConfig)
	}

	client := mqtt.NewClient(opts)

	backoff.Retry(func() error {
		log.Printf("connect attempt to %s", opts.Servers[0])
		token := client.Connect()
		token.Wait()
		return token.Error()
	}, backoff.NewExponentialBackOff())

	log.Printf("connected")

	return client
}

func sineBetween(min float64, max float64, t int) float64 {
	msInDay := 60 * 60 * 24
	periodThroughDay := float64(t%msInDay) / float64(msInDay)
	return min + math.Sin(periodThroughDay*math.Pi)*(max-min)
}

func main() {
	var config Config
	err := envconfig.Process("ENV", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Printf("%+#v", config)

	client := NewMqttClient(&config)

	for _ = range time.Tick(time.Second * 60) {
		// example message payload construction
		messageTimestamp := time.Now().UTC()
		messageTimestampUnix := time.Now().Unix()

		exampleTadTemperatureReading := sineBetween(14, 26, int(messageTimestampUnix)) // degrees
		exampleTadBatteryReading := sineBetween(2000, 4000, int(messageTimestampUnix)) // mAh
		payload := fmt.Sprintf(`
{
	"guid": "%s",
	"timestamp": "%s",
	"measurements": [
		{
			"name": "temperature",
			"data": {
				"n-value": %f
			}
		},
		{
			"name": "battery",
			"data": {
				"i-voltage": %d
			}
		},
		{
			"name": "location",
			"data": {
				"n-latitude": %f,
				"n-longitude": %f
			}
		}

	]
}`,
			config.DeviceId,
			messageTimestamp.Format("2006/01/02,15:04:05"), // second precision
			exampleTadTemperatureReading,
			int(exampleTadBatteryReading),
			-37.7774628,
			175.3079456,
		)

		// mqtt publishing parameters
		publishTopic := fmt.Sprintf("device/%s/nb/json", config.DeviceId)
		isRetained := false

		token := client.Publish(publishTopic, config.PublishQos, isRetained, []byte(payload))
		token.Wait()

		if err := token.Error(); err != nil {
			log.Fatalf("failed to publish to %s: %s", publishTopic, err)
		}

		log.Printf("published message to %s (qos = %d)", publishTopic, config.PublishQos)
	}
}
